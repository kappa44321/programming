# все четные цифры числа

a = int(input("Введите число: "))


def ev(x):
    while x > 0:
        if (x % 10) % 2 == 0:
            print(x % 10, end=" ")
        x //= 10


print("Ответ: ", end="")

ev(a)
