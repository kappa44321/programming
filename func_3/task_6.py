# сумма нечетных цифр числа

a = int(input("Введите число: "))


def odd(x):
    sum = 0

    while x > 0:
        if (x % 10) % 2 != 0:
            sum += x % 10
        x //= 10
    return sum


print("Ответ:", odd(a), end="")
